const users = require("./users");
const default_teams = require("./teams");

function getTeamNameFromUsername(username) {
  const name = (
    username
      .replace(/[\])}[{(]/g, " ")
      .replace(/\s\s+/g, " ")
      .split(" ")
      .slice(2)
      .join("") || ""
  )
    .replace(/[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/gi, "")
    .toLowerCase();
  if (/saitama|technology|swat|swatdee|valkyrie|tech/.test(name)) {
    return "Tech";
  } else if (/imp|improvement|goose|^hero$|hero1/.test(name)) {
    return "Hero 1";
  } else if (/wongsawang|วงศ์สว่าง|แยกวงศ์สว่าง|hero2/.test(name)) {
    return "Hero 2";
  } else if (/rocket|discover|discovery|navigation|home/.test(name)) {
    return "Today";
  } else if (/kabigon|player|livestream|livestreamplayer|watch/.test(name)) {
    return "Watch";
  } else if (/content|drama|krypto|listen/.test(name)) {
    return "Listen";
  } else if (/read/.test(name)) {
    return "Read";
  } else if (/subscription/.test(name)) {
    return "Subscription";
  } else if (/community/.test(name)) {
    return "Community";
  } else if (/communicator|communication|ncc|thor/.test(name)) {
    return "Communicator";
  } else if (/yoda|privilege|trueyou|redeem/.test(name)) {
    return "Privilege";
  } else if (/commerce/.test(name)) {
    return "Commerce";
  } else {
    return undefined;
  }
}

function getTeamNameFromUuid(uuid) {
  let res = users.find((user) => {
    return user.bitbucket == uuid;
  });
  return res != undefined ? res.teamname : undefined;
}

module.exports.getUrl = function getUrl(string, repositoryUrl) {
  let prMatch = / PR-(\d+)/.exec(string);
  if (prMatch !== null) {
    return "https://bitbucket.org/truedmp/trueid-android-v3/pull-requests/" + prMatch[1];
  }

  let branchMatch = /trueid-android-v3.*? » (.+) #(\d+)/.exec(string);
  if (branchMatch !== null) {
    return repositoryUrl + "/branch/" + branchMatch[1];
  }

  return undefined;
};

module.exports.getTeamName = function getTeamName(user) {
  return (
    getTeamNameFromUuid(user.uuid) ||
    getTeamNameFromUsername(user.display_name) ||
    getTeamNameFromUsername(user.nickname)
  );
};

module.exports.getTeamNameFromUuid = getTeamNameFromUuid;

module.exports.getDiscordId = function getDiscordId(uuid) {
  let res = users.find((user) => {
    return user.bitbucket == uuid;
  });
  return res != undefined ? res.discord : undefined;
};

module.exports.getGoogleId = function getGoogleId(uuid) {
  let res = users.find((user) => {
    return user.bitbucket == uuid;
  });
  return res != undefined ? res.google : undefined;
};

module.exports.getTeamNameFromUsername = getTeamNameFromUsername;

String.prototype.trunc =
  String.prototype.trunc ||
  function (length) {
    return this.length > length ? this.substr(0, length - 1) + "\n..." : this;
  };
