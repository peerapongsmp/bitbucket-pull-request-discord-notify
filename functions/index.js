const functions = require("firebase-functions");
const { user } = require("firebase-functions/lib/providers/auth");

const alert_build = require("./alert_build");
const alert_pull_request = require("./alert_pull_request");
const alert_pull_request_comment = require("./alert_pull_request_comment");
const alert_new_pull_request = require("./alert_new_pull_request");

const GOOGLE_WEBHOOK_API_ANDROID =
  "https://chat.googleapis.com/v1/spaces/AAAAV7TEzv0/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=1ji2Q4h3snN7kCMRHqhS8mkKku09P7X7VZ0RwYBqiNo%3D";
const GOOGLE_WEBHOOK_API_IOS =
  "https://chat.googleapis.com/v1/spaces/AAAAdVexsFU/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=CUsD9TN_9985Fdvc67XFLUX1Kri__VyePwwZBtAogrQ%3D";
const GOOGLE_WEBHOOK_API_ME =
  "https://chat.googleapis.com/v1/spaces/AAAAUfhW9u8/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=JIyh6QnJXxbVhweSjx8DXgyz8BAAHgKjzaJCDlaqhLU%3D";

const GOOGLE_WEBHOOK_API_PR =
  "https://chat.googleapis.com/v1/spaces/AAAAROPL04E/messages?key=AIzaSyDdI0hCZtE6vySjMm-WEfRq3CPzqKqqsHI&token=63m4yUJfA6LKKxcb38qOQCSdt0-Ej3TZr2lEHIWyoyU%3D";

exports.FN_Hook_Alert_PR_to_Discord = functions.https.onRequest((req, res) => {
  const event = req.header("x-event-key");

  var platform;

  if (
    event == undefined ||
    (event != "repo:commit_status_updated" &&
      event.indexOf("pullrequest:") == -1)
  ) {
    return res.status(200).send("Invalid operation");
  }

  let googleApiUrl;
  let prAlertApiUrl = GOOGLE_WEBHOOK_API_PR;

  if (req.path != undefined && req.path.indexOf("/ios") == 0) {
    googleApiUrl = GOOGLE_WEBHOOK_API_IOS;
    platform = "iOS";
  } else if (req.path != undefined && req.path.indexOf("/android") == 0) {
    googleApiUrl = GOOGLE_WEBHOOK_API_ANDROID;
    platform = "Android";
  } else if (req.path != undefined && req.path.indexOf("/me") == 0) {
    googleApiUrl = GOOGLE_WEBHOOK_API_ME;
    prAlertApiUrl = GOOGLE_WEBHOOK_API_ME;
    platform = "Android";
  } else {
    return res.status(200).send("Invalid parameter");
  }

  try {
    if (event.indexOf("pullrequest:comment_created") != -1) {
      alert_pull_request_comment(event, req.body, googleApiUrl)
        .then(() => {
          return res.status(200).send("Done");
        })
        .catch(function (err) {
          return res.status(200).send(err.message);
        });
    } else if (event.indexOf("pullrequest:") != -1) {
      if (event.indexOf("pullrequest:created") != -1) {
        alert_new_pull_request(req, prAlertApiUrl);
      }

      alert_pull_request(event, req.body, googleApiUrl, platform)
        .then(() => {
          return res.status(200).send("Done");
        })
        .catch(function (err) {
          return res.status(200).send(err.message);
        });
    } else if (
      event === "repo:commit_status_updated" &&
      (req.body.commit_status.state === "FAILED" ||
        req.body.commit_status.state === "SUCCESSFUL")
    ) {
      alert_build(event, req.body, googleApiUrl)
        .then(() => {
          return res.status(200).send("Done");
        })
        .catch(function (err) {
          return res.status(200).send(err.message);
        });
    } else {
      return res.status(200).send("Invalid operation");
    }
  } catch (err) {
    return res.status(200).send(err.message);
  }
});
