const fetch = require("node-fetch");

module.exports = (commitUrl, message, apiUrl, platform) => {
  return fetch(`${commitUrl.href}/statuses`, {
    method: "GET",
    headers: {
      Authorization:
        "Basic dGVjaF9tb2JpbGVfZW5naW5lZXJpbmdfMjpuTU41d1ZqS1VqakxCTUN1c2tQ==",
      "Content-Type": "application/json",
    },
  })
    .then((response) => response.json())
    .then((json) => {
      var isBuildSuccessful =
        json.values.every((status) => status.state == "SUCCESSFUL") &&
        json.values.length > 0;
      if (isBuildSuccessful) {
        console.log(JSON.stringify(message));
        return fetch(apiUrl, {
          method: "POST",
          headers: {
            "Content-Type": "application/json",
          },
          body: JSON.stringify(message),
        })
          .then((response) => console.log("SUCCESS"))
          .catch((error) => console.log(error));
      }
    })
    .catch((error) => {
      return fetch(apiUrl, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify(message),
      })
        .then((response) => console.log("SUCCESS"))
        .catch((error) => console.log(error));
    });
};
