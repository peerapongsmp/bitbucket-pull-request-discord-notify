const fetch = require("node-fetch");
const helper = require("./helper");

module.exports = (req, apiUrl) => {
  let pullrequest = req.body.pullrequest;
  let mentionName;

  if (req.path != undefined && req.path.indexOf("/ios") == 0) {
    mentionName = ` *@iOS Devs* `;
  } else if (req.path != undefined && req.path.indexOf("/android") == 0) {
    mentionName = `🤖 *@Android Devs* `;
  } else if (req.path != undefined && req.path.indexOf("/me") == 0) {
    mentionName = `🤖 *Android Devs* `;
  }

  let message = `<users/all> ${mentionName}ฝาก Pull Request ด้วยครับ\n${pullrequest.title}\n${pullrequest.links.html.href}\n`;

  const googleId = helper.getGoogleId(pullrequest.author.uuid);
  if (googleId != undefined) {
    message += `By. <users/${googleId}>\n`;
  }

  let messages = {
    text: message,
  };

  return fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(messages),
  });
};
