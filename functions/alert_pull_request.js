const fetch = require("node-fetch");
const default_teams = require("./teams");
const helper = require("./helper");
const alert_pull_request_ready_to_merge = require("./alert_pull_request_ready_to_merge");

module.exports = (event, body, apiUrl, platform) => {
  let event_name = event
    .replace("pullrequest:", "")
    .replace("fulfilled", "merged")
    .replace("rejected", "declined");

  let pullrequest = body.pullrequest;
  let actor = body.actor;

  const header = {
    title: pullrequest.author.display_name,
    imageUrl: pullrequest.author.links.avatar.href,
    imageStyle: "AVATAR",
  };

  const widgets = getWidgets(event_name, pullrequest);

  let messages = {
    text:
      ({
        created: `🔥 `,
        approved: `👍 `,
        unapproved: `👎 `,
        merged: `🎉 `,
      }[event_name] || ``) +
      `<${actor.links.html.href}|${actor.display_name}> ${event_name} pull request <${pullrequest.links.html.href}|#${pullrequest.id}>`,
    cards: [
      {
        header: header,
        sections: [
          {
            widgets: widgets,
          },
        ],
      },
    ],
  };

  const mergeMessage = getMergeMessage(
    event_name,
    getApprovalTeams(pullrequest.participants, pullrequest.author),
    pullrequest
  );

  if (mergeMessage != undefined) {
    alert_pull_request_ready_to_merge(
      pullrequest.source.commit.links.self,
      {
        text: mergeMessage,
        cards: [
          {
            header: header,
            sections: [
              {
                widgets: [
                  ...widgets,
                  ...[
                    {
                      buttons: [
                        {
                          textButton: {
                            text: "Merge Pull Request",
                            onClick: {
                              openLink: {
                                url: pullrequest.links.html.href,
                              },
                            },
                          },
                        },
                      ],
                    },
                  ],
                ],
              },
            ],
          },
        ],
      },
      apiUrl,
      platform
    );
  }

  return fetch(apiUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(messages),
  });
};

function isNoChangesRequested(participants) {
  return !participants.some(
    (participant) => participant.state == "changes_requested"
  );
}

function getApprovedParticipants(participants) {
  const approvedParticipants = participants.filter(
    (participant) => participant.approved
  );
  return approvedParticipants.length > 0
    ? [
        {
          keyValue: {
            topLabel: `${approvedParticipants.length} Approved`,
            content: approvedParticipants
              .map((participant) => {
                return `✅ ${participant.user.display_name}`;
              })
              .sort((a, b) => {
                a.localeCompare(b);
              })
              .join(", "),
            contentMultiline: true,
          },
        },
      ]
    : [];
}

function getChangeRequested(participants) {
  const changesRequestedParticipants = participants.filter(
    (participant) => participant.state == "changes_requested"
  );
  return changesRequestedParticipants.length > 0
    ? [
        {
          keyValue: {
            topLabel: `${changesRequestedParticipants.length} Changes Requested`,
            content: changesRequestedParticipants
              .map((participant) => `⛔️ ${participant.user.display_name}`)
              .join(", "),
            contentMultiline: true,
          },
        },
      ]
    : [];
}

function getApprovalTeams(participants, author) {
  return [
    ...participants
      .filter(
        (participant) =>
          participant.user.uuid != author.uuid && participant.approved
      )
      .map((participant) => ({
        name: helper.getTeamName(participant.user),
        approved: participant.approved,
      })),
    ...default_teams,
  ]
    .filter((team) => team.name != undefined)
    .reduce(
      (allteams, currentteam) =>
        !allteams.find((team) => team.name === currentteam.name)
          ? allteams.concat([currentteam])
          : allteams,
      []
    )
    .sort(function (a, b) {
      return b.approved - a.approved || a.name.localeCompare(b.name);
    });
}

function getMergeMessage(event_name, teams, pullrequest) {
  let mergeMessage;
  const googleId = helper.getGoogleId(pullrequest.author.uuid);
  if (
    (event_name == "approved" ||
      event_name == "unapproved" ||
      event_name == "changes_request_removed" ||
      event_name == "changes_request_created") &&
    googleId != undefined &&
    pullrequest.state === "OPEN"
  ) {
    const approvedTeams = teams.filter((team) => team.approved).length;
    const noCR = isNoChangesRequested(pullrequest.participants);
    if (
      (/Tech|Hero/.test(helper.getTeamName(pullrequest.author)) &&
        approvedTeams >= 8 &&
        noCR) ||
      (approvedTeams >= 6 && noCR)
    ) {
      mergeMessage = `<users/${googleId}> your pull request <${pullrequest.links.html.href}|#${pullrequest.id}> is ready to merge`;
    }
  }

  return mergeMessage;
}

function getApprovedTeams(teams) {
  const approvedTeams = teams.filter((team) => team.approved);
  return approvedTeams.length > 0
    ? [
        {
          keyValue: {
            topLabel: `${approvedTeams.length} Teams Approved`,
            content: approvedTeams.map((team) => "✅ " + team.name).join(", "),
            contentMultiline: true,
          },
        },
      ]
    : [];
}

function getWatingApprovalTeams(teams) {
  const waitingApprovalTeams = teams.filter(
    (team) => !team.approved && team.name.indexOf("Ext-") == -1
  );
  return waitingApprovalTeams.length > 0
    ? [
        {
          keyValue: {
            topLabel: `Waiting for Approval from ${waitingApprovalTeams.length} Teams `,
            content: waitingApprovalTeams
              .map((team) => "⬜️ " + team.name)
              .join(", "),
            contentMultiline: true,
          },
        },
      ]
    : [];
}

function getWidgets(event_name, pullrequest) {
  return [
    {
      keyValue: {
        content: pullrequest.title,
        bottomLabel: `${pullrequest.source.repository.name}:${pullrequest.source.branch.name} → ${pullrequest.destination.repository.name}:${pullrequest.destination.branch.name}`,
        contentMultiline: true,
      },
    },
    ...((event_name == "created" || event_name == "updated") &&
    pullrequest.description != ""
      ? [
          {
            keyValue: {
              topLabel: "Description",
              content: pullrequest.description.trunc(500),
              contentMultiline: true,
            },
          },
        ]
      : []),
    ...getApprovedParticipants(pullrequest.participants),
    ...getChangeRequested(pullrequest.participants),
    ...(pullrequest.participants.filter((participant) => participant.approved)
      .length > 0
      ? (function () {
          const approvalTeams = getApprovalTeams(
            pullrequest.participants,
            pullrequest.author
          );

          return [
            ...getApprovedTeams(approvalTeams),
            ...getWatingApprovalTeams(approvalTeams),
          ];
        })()
      : []),
  ];
}
