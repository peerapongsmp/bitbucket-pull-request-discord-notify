const fetch = require("node-fetch");
const users = require("./users");
const default_teams = require("./teams");
const helper = require("./helper");
const alert_pull_request_ready_to_merge_google_chat = require("./alert_pull_request_ready_to_merge");

module.exports = (event, body, endpointUrl) => {
  let event_name = event
    .replace("pullrequest:", "")
    .replace("fulfilled", "merged")
    .replace("rejected", "declined");

  let pullrequest = body.pullrequest;
  let actor = body.actor;

  let messages = {
    text:
      ({
        created: `🔥 `,
        approved: `👍 `,
        unapproved: `👎 `,
        merged: `🎉 `,
      }[event_name] || "") +
      `<${actor.links.html.href}|${actor.display_name}> ${event_name} pull request <${pullrequest.links.html.href}|#${pullrequest.id}>`,
    cards: [
      {
        header: {
          title: pullrequest.author.display_name,
          imageUrl: pullrequest.author.links.avatar.href,
          imageStyle: "AVATAR",
        },
        sections: [
          {
            widgets: [
              {
                keyValue: {
                  content: pullrequest.title,
                  bottomLabel: `<i>${pullrequest.source.repository.name}:${pullrequest.source.branch.name} → ${pullrequest.destination.repository.name}:${pullrequest.destination.branch.name}</i>`,
                  contentMultiline: true,
                },
              },
              {
                keyValue: {
                  topLabel: "Content",
                  content: body.comment.content.html.replace(
                    /<\/?[^>]+(>|$)/g,
                    ""
                  ),
                  contentMultiline: true,
                },
              },
              {
                buttons: [
                  {
                    textButton: {
                      text: "View Comment on Bitbucket",
                      onClick: {
                        openLink: {
                          url: body.comment.links.html.href,
                        },
                      },
                    },
                  },
                ],
              },
            ],
          },
        ],
      },
    ],
  };

  return fetch(endpointUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(messages),
  });
};
