const fetch = require("node-fetch");
const helper = require("./helper");

module.exports = (event, body, endpointUrl) => {
  const commit_status = body.commit_status;
  const repository = body.repository;

  if (commit_status.name.indexOf("Quality Code Check") === -1) {
    throw new Error("Support only Quality Code Check");
  }

  const url = helper.getUrl(commit_status.name, repository.links.html.href);

  let googleId;
  if (
    typeof commit_status.commit.author === "undefined" ||
    typeof commit_status.commit.author.user === "undefined" ||
    typeof commit_status.commit.author.user.uuid === "undefined"
  ) {
    googleId = null;
  } else {
    googleId = helper.getGoogleId(commit_status.commit.author.user.uuid);
  }

  var randomNumber = Math.floor(Math.random() * 2);

  const message = {
    failed: [
      "ว้ายยย บิ้วพัง แก้ด้วยจ้า",
      "บิลด์ไม่ผ่านจ้า",
      "แป่ว บิลด์ไม่ผ่าน",
    ][randomNumber],
    successful: [
      "บิ้วผ่าน รอดแล้วโว้ย",
      "บิลด์ผ่าน ดีใจด้วย",
      "บิลด์ผ่าน คำที่ไม่ได้ยินมาเนิ่นนาน",
    ][randomNumber],
  }[commit_status.state.toLowerCase()];

  const imageUrl = {
    failed: [
      "https://media.makeameme.org/created/build-failed-6603e383f3.jpg",
      "https://media.makeameme.org/created/your-build-failed.jpg",
      "https://i.imgflip.com/1cjnmv.jpg",
    ][randomNumber],
    successful: [
      "https://www.meme-arsenal.com/memes/cb5911eebc9cdd8956a49d5a25ae64d3.jpg",
      "https://i.imgflip.com/5brs6a.jpg",
      "https://i.imgflip.com/5brt1l.jpg",
    ][randomNumber],
  }[commit_status.state.toLowerCase()];

  let displayName;
  if (
    typeof commit_status.commit.author === "undefined" ||
    typeof commit_status.commit.author.user === "undefined" ||
    typeof commit_status.commit.author.user.display_name === "undefined"
  ) {
    displayName = "";
  } else {
    displayName = commit_status.commit.author.user.display_name;
  }

  const messagePayload = {
    text: `${googleId ? `<users/${googleId}> ` : `${displayName} `}
    ${message} <${url ? url : commit_status.url}|${commit_status.name}>`,
    cards: [
      {
        sections: [
          {
            widgets: [
              {
                keyValue: {
                  topLabel: "Commit Message",
                  content: commit_status.commit.message,
                  contentMultiline: true,
                },
              },
              {
                keyValue: {
                  topLabel: "Build Status",
                  content: commit_status.state,
                  contentMultiline: true,
                },
              },
              {
                buttons: [
                  {
                    textButton: {
                      text: "View Commit",
                      onClick: {
                        openLink: {
                          url: commit_status.commit.links.html.href,
                        },
                      },
                    },
                  },
                  {
                    textButton: {
                      text: "View Jenkins Build",
                      onClick: {
                        openLink: {
                          url: `https://httpbin.org/redirect-to?url=${commit_status.url}`,
                        },
                      },
                    },
                  },
                  {
                    textButton: {
                      text: "View Pull Request",
                      onClick: {
                        openLink: {
                          url: url,
                        },
                      },
                    },
                  },
                ],
              },
              {
                image: {
                  imageUrl: imageUrl,
                },
              },
            ],
          },
        ],
      },
    ],
  };

  return fetch(endpointUrl, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(messagePayload),
  });
};
